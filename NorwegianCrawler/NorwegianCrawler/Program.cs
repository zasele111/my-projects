﻿using CrawlerNorwegian2;
using HtmlAgilityPack;
using RestSharp;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using System.Xml;

namespace CrawlerNorwegian
{
    class Program
    {
        static void Main(string[] args)
        {
            var crawler = new Crawler();
            for (int i = 1; i < 31; i++)
            {
                Task.Run(async () => { await crawler.StartCrawlerAsync(i); }).GetAwaiter().GetResult();
            }
            Console.WriteLine("done");

            Console.ReadLine();
        }


    }
}
