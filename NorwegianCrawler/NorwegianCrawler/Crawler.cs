﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using HtmlAgilityPack;
using RestSharp;

namespace CrawlerNorwegian2
{
    public class Crawler
    {
        private const string Pattern = @"<[^>]*?>";
        private const string PatternByDivFirst = "<div.*?>(.*?)<\\/div>";
        private const string PatternByDivSecond = @"<div[^>]*?>(.*?)</div>";
        private const string PatternByLabel = "<label.*?>(.*?)<\\/label>";
        private const string PatternBySpan = "<span.*?>(.*?)<\\/span>";

        private readonly HttpClient _httpClient;
        private readonly RestClient _restClient;

        public Crawler()
        {
            _httpClient = new HttpClient();
            _restClient = new RestClient();
        }

        private List<string> GetRowElements(string rowInformation, string pattern)
        {
            Regex.Replace(rowInformation, Pattern, "");

            var matchesFirstRow = Regex.Matches(rowInformation, pattern,
                RegexOptions.IgnoreCase | RegexOptions.Singleline);

            var rowElements = new List<string>();
            foreach (Match match in matchesFirstRow)
            {
                rowElements.Add(match.Value);
            }

            return rowElements;
        }

        public async Task StartCrawlerAsync(int day)
        {
            var flights = new List<Flight>();

            var dayNumber = day < 10 ? "0" + day : day.ToString();
            var url =
                $"https://www.norwegian.com/en/ipc/availability/avaday?A_City=RIX&AdultCount=1&ChildCount=0&CurrencyCode=EUR&D_City=OSLALL&D_Day={dayNumber}&D_Month=201804&D_SelectedDay=01&IncludeTransit=true&InfantCount=0&R_Day=08&R_Month=201803&TripType=1&mode=ab";
            _restClient.BaseUrl = new Uri(url);
            var request = new RestRequest(Method.GET);
            var response = await _restClient.ExecuteGetTaskAsync(request);
            var htmlDocument = new HtmlDocument();
            htmlDocument.LoadHtml(response.Content);
            var viewState = htmlDocument.DocumentNode
                .SelectNodes("//div[contains(@class, 'nas-components')]//input")
                .First().Attributes["value"].Value;
            var firstRowInformation = new List<HtmlNode>();
            try
            {
                firstRowInformation =
                    htmlDocument.DocumentNode.SelectNodes("//tr[contains(@class, 'rowinfo1')]").ToList();
            }
            catch (Exception)
            {
               // Console.WriteLine($"No flights for {day} day found");
                return;
            }

            var secondRowInformation =
                htmlDocument.DocumentNode.SelectNodes("//tr[contains(@class, 'rowinfo2')]").ToList();
            var thirdRowInformation = htmlDocument.DocumentNode.SelectNodes("//tr[contains(@class, 'row lastrow')]")
                .ToList();
            var flightNumbers =
                htmlDocument.DocumentNode.SelectNodes(
                    "//td[contains(@class, 'fareselect standardlowfare')]//input");

            var flightNumberList = new List<string>();
            for (int i = 0; i < flightNumbers.Count; i = i + 2)
            {
                flightNumberList.Add(flightNumbers.ElementAt(i).Attributes["value"].Value);
            }

            var connectionFlightsAirports = thirdRowInformation.Select((t, i)
                => Regex.Replace(thirdRowInformation.ElementAt(i).InnerHtml, Pattern, "")).ToList();

            //firstRow count = flights per day count
            for (var flightIndex = 0; flightIndex < firstRowInformation.Count; flightIndex++)
            {
                var flight = new Flight();

                //Parse first row info
                var addInformationByNumber = 0;
                addInformationByNumber = ParseFirstRow(firstRowInformation, flightIndex,
                    addInformationByNumber, flight, connectionFlightsAirports);

                //Parse second row info
                ParseSecondRow(secondRowInformation, flightIndex, addInformationByNumber, flight);

                //Get taxes for flight              
                GetTaxesForFlight(url, flightIndex, flightNumberList, viewState, flight, flights);

            }
            OutputFlightsToCurrentDirectory(day, flights);
        }

        private static void OutputFlightsToCurrentDirectory(int day, List<Flight> flights)
        {
            var path = Directory.GetCurrentDirectory() + "\\flights.txt";
            if (!File.Exists(path))
            {
                using (var sw = File.CreateText(path))
                {
                    sw.WriteLine("Flights information from Oslo to Riga in April, 2018");
                }
            }

            using (var sw = File.AppendText(path))
            {
                sw.WriteLine();
                sw.WriteLine("April : " + day);

                for (int i = 0; i < flights.Count; i++)
                {
                    if (flights.ElementAt(i).FlightType == "Direct")
                    {
                        sw.WriteLine("Departure airPort: " + flights.ElementAt(i).DepartureAirport);
                        sw.WriteLine("Departure time: " + flights.ElementAt(i).DepartureTime);
                        sw.WriteLine("Arrival airPort: " + flights.ElementAt(i).ArrivalAirport);
                        sw.WriteLine("Arrival time: " + flights.ElementAt(i).ArrivalTime);
                        sw.WriteLine("Price: " + flights.ElementAt(i).Price);
                        sw.WriteLine("Taxes: " + flights.ElementAt(i).Taxes);
                        sw.WriteLine();
                    }
                }
            }
        }

        private void GetTaxesForFlight(string url, int flightIndex, List<string> flightNumberList, string viewState,
            Flight flight, List<Flight> flights)
        {
            _restClient.BaseUrl = new Uri(url);
           // Console.WriteLine(flightIndex);
            var request = new RestRequest(Method.POST);
            request.AddHeader("Content-Type", "application/x-www-form-urlencoded");
            request.AddParameter("__EVENTTARGET",
                "ctl00_MainContent_ipcAvaDay_ipcResultOutbound_lbtPostBackFaker"); // adds to POST or URL querystring based on Method
            if (flightIndex < flightNumberList.Count)
            {
                request.AddParameter("__EVENTARGUMENT",
                    flightNumberList
                        .ElementAt(flightIndex)); // replaces matching token in request.Resource flightNumberList.ElementAt(2*p) "0|DY1072OSLRIX|1|0|0"
                request.AddParameter("__VIEWSTATE", viewState);
                var response = _restClient.Execute(request);
                var split = Regex.Split(response.Content, "rightcell emphasize");

                for (int i = 2; i < split.Length; i++)
                {
                    var taxPrice = Regex.Split(response.Content, "rightcell emphasize").ElementAt(i).Substring(33, 5);
                    flight.Taxes = taxPrice;
                  //  Console.WriteLine(taxPrice);
                }
                flights.Add(flight);
            }
        }

        private int ParseFirstRow(List<HtmlNode> firstRowInformation, int flightIndex, int addInformationByNumber, Flight flight,
            List<string> connectionFlightsAirports)
        {
            var firstRowHtmlElements = GetRowElements(firstRowInformation.ElementAt(flightIndex).InnerHtml, PatternByDivSecond);
            for (int i = 0; i < firstRowHtmlElements.Count; i++)
            {
                var element = firstRowHtmlElements.ElementAt(i);
                var divMatches = Regex.Matches(element, PatternByDivFirst);
                if (divMatches.Count > 0)
                    for (int k = 0; k < divMatches.Count; k++)
                    {
                        //Check if arrival time has +1 day
                        var match = divMatches[k];
                        if (match.Length > 56)
                        {
                            var arrivalTimeWithoutDay = match.Groups[1].ToString().Substring(0, 5);
                            var spanMatches = Regex.Matches(element, PatternBySpan);
                            foreach (Match spanMatch in spanMatches)
                            {
                                addInformationByNumber = addInformationByNumber + 1;
                                flight.ArrivalTime = arrivalTimeWithoutDay + " " + spanMatch.Groups[1];
                            }
                        }
                        else
                        {
                            addInformationByNumber = addInformationByNumber + 1;
                            //Conecction flight
                            var flightTypeAndTime = match.Groups[1].ToString();
                            if (flightTypeAndTime != "Direct" && addInformationByNumber == 3)
                            {
                                if (connectionFlightsAirports.ElementAt(0).Length < 30)
                                {
                                    var splitSentenceByElement =
                                        Regex.Split(connectionFlightsAirports.ElementAt(0), "in ");
                                    var connectionFlightInfo = splitSentenceByElement.ToList();
                                    flight.ConnectionAirport = connectionFlightInfo.ElementAt(1);
                                }
                                else if (connectionFlightsAirports.ElementAt(0).Length < 80)
                                {
                                    var splitSentenceByElement = connectionFlightsAirports.ElementAt(0).Split(' ');
                                    var connectionFlightInfo = splitSentenceByElement.ToList();
                                    flight.ConnectionAirport = connectionFlightInfo.ElementAt(7);
                                }
                                else
                                {
                                    flight.ConnectionAirport = "WithoutConnectionFlight";
                                }
                            }

                            //Flight time
                            switch (addInformationByNumber)
                            {
                                case 1:
                                    flight.DepartureTime = flightTypeAndTime;
                                    break;
                                case 2:
                                    flight.ArrivalTime = flightTypeAndTime;
                                    break;
                                case 3:
                                    flight.FlightType = flightTypeAndTime;
                                    break;
                            }
                        }
                    }

                if (i == 3)
                {
                    break;
                }
            }

            for (int i = 0; i < firstRowHtmlElements.Count; i++)
            {
                var matches = Regex.Matches(firstRowHtmlElements.ElementAt(i), PatternByLabel);
                if (matches.Count > 0)
                    foreach (Match match in matches)
                    {
                        addInformationByNumber = addInformationByNumber + 1;
                        if (addInformationByNumber == 4)
                        {
                            flight.Price = Convert.ToString(match.Groups[1]);
                        }
                    }
            }
            return addInformationByNumber;
        }

        private void ParseSecondRow(List<HtmlNode> secondRowInformation, int flightIndex, int addInformationByNumber, Flight flight)
        {
            var secondRowHtmlElements =
                GetRowElements(secondRowInformation.ElementAt(flightIndex).InnerHtml, PatternByDivSecond);
            for (int i = 0; i < secondRowHtmlElements.Count; i++)
            {
                var matches = Regex.Matches(secondRowHtmlElements.ElementAt(i), PatternByDivFirst);
                if (matches.Count > 0)
                {
                    foreach (Match match in matches)
                    {
                        if (match.Length < 100)
                        {
                            addInformationByNumber = addInformationByNumber + 1;
                            var airport = match.Groups[1].ToString();
                            switch (addInformationByNumber)
                            {
                                case 7:
                                    flight.DepartureAirport = airport;
                                    break;
                                case 8:
                                    flight.ArrivalAirport = airport;
                                    break;
                            }
                        }
                    }
                }
            }
        }
    }
}