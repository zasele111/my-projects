﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CrawlerNorwegian2
{
    public class Flight
    {
        public string DepartureAirport { get; set; }
        public string ArrivalAirport { get; set; }
        public string DepartureTime { get; set; }
        public string ArrivalTime { get; set; }
        public string ConnectionAirport { get; set; }
        public string Price { get; set; }
        public string FlightType { get; set; }
        public string Taxes { get; set; }

    }
}
