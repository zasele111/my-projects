import java.util.ArrayList;

public class Main {

    public static void main(String[] args) {
        Branch mainBranch = new Branch();
        Branch branch2= new Branch();
        Branch branch3 = new Branch();
        Branch branch4 = new Branch();
        Branch branch5 = new Branch();
        Branch branch6 = new Branch();
        Branch branch7 = new Branch();
        Branch branch8 = new Branch();
        Branch branch9 = new Branch();
        Branch branch10 = new Branch();
        Branch branch11 = new Branch();

        // Main Branch has 2 smaller branches (depth level 2)

        mainBranch.Branches.add(branch2);
        mainBranch.Branches.add(branch3);
        // Left Branch has 1 smaller branch (depth level 3)
        branch2.Branches.add(branch4);
        // Right Branch has 3 smaller branches (depth level 3)
        branch3.Branches.add(branch5);
        branch3.Branches.add(branch6);
        branch3.Branches.add(branch7);
        // Left Branch has 1 smaller branch (depth level 4)
        branch5.Branches.add(branch8);
        // Middle Branch has 2 smaller branches (depth level 4)
        branch6.Branches.add(branch9);
        branch6.Branches.add(branch10);
        // Left Branch has 1 smaller branch (depth level 5)
        branch9.Branches.add(branch11);

        System.out.println("Lowest level is "+Branch.GetDepth(mainBranch));

        //                                                 Another option which is simple to understand

        Branch level1 = new Branch();
        Branch level2 = new Branch();
        Branch level3 = new Branch();
        Branch level4 = new Branch();
        Branch level5 = new Branch();
        Branch level6 = new Branch();
        Branch level7 = new Branch();
        Branch level8 = new Branch();
        Branch level9 = new Branch();
        Branch level10 = new Branch();
        Branch level11 = new Branch();

        level1.Branches.add(level2);
        level2.Branches.add(level3);
        level3.Branches.add(level4);
        level4.Branches.add(level5);
        level5.Branches.add(level6);
        level6.Branches.add(level7);
        level7.Branches.add(level8);
        level8.Branches.add(level9);
        level9.Branches.add(level10);
        level10.Branches.add(level11);

        System.out.println("Lowest level is "+Branch.GetDepth(level1));
    }
}
