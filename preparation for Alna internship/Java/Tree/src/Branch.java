import java.util.ArrayList;
public class Branch {
    public ArrayList<Branch> Branches;

    public void setBranches(ArrayList<Branch> branches){
        this.Branches = branches;
    }

    public ArrayList<Branch> getBranches() {
        return Branches;
    }

    public Branch()
    {
        Branches = new ArrayList<Branch>();
    }

    public static int GetDepth(Branch branch)
    {
        int depth = 0;
        for (Branch node : branch.Branches)
        {
            int tmp = GetDepth(node);
            if (tmp > depth)
            {
                depth = tmp;
            }
        }
        depth++;
        return depth;
    }
}
